
const showMoreStyles = (trigger, styles) => {
    const cards = document.querySelectorAll(styles),
          btn = document.querySelector(trigger);

    cards.forEach(card => {
        card.classList.add('animate__animated', 'animate__fadeInUp');
    });

    btn.addEventListener('click', () => {
        cards.forEach(card => {
            card.classList.remove('xl-hidden', 'lg-hidden', 'md-hidden', 'sm-hidden');
            card.classList.add('col-md-3', 'col-sm-12', 'col-12');
        });
        // btn.style.display = 'none';
        btn.remove();
    });
};

export default showMoreStyles;


// VAR 2

// import {getResource} from '../services/requests';

// const showMoreStyles = (trigger, wrapper) => {
//     const btn = document.querySelector(trigger);

//     btn.addEventListener('click', function() {
//         getResource('db.json')
//             .then(res => createCards(res.styles))
//             .catch(error => console.log(error));

//         this.remove();
//     });

//     function createCards(response) {
//         response.forEach(({src, title, link}) => {
//             let card = document.createElement('div');

//             card.classList.add('animate__animated', 'animate__fadeInUp', 'col-md-3', 'col-sm-12', 'col-12');
        
//             card.innerHTML = `
//                 <div class="styles-block">
//                     <img src=${src} alt="style">
//                     <h4>${title}</h4>
//                     <a href=${link}>Подробнее</a>
//                 </div>
//             `;

//             document.querySelector(wrapper).appendChild(card);
//         });
//     };
// };

//VAR 3

// const showMoreStyles = (trigger, wrapper) => {
//     const btn = document.querySelector(trigger);

//     btn.addEventListener('click', function() {
//         getResource('http://localhost:3000/styles')
//             .then(res => createCards(res))
//             .catch(error => console.log(error));

//         this.remove();
//     });

//     function createCards(response) {
//         response.forEach(item => {
//             let card = document.createElement('div');

//             card.classList.add('animate__animated', 'animate__fadeInUp', 'col-md-3', 'col-sm-12', 'col-12');
        
//             card.innerHTML = `
//                 <div class="styles-block">
//                     <img src=${item.src} alt="style">
//                     <h4>${item.title}</h4>
//                     <a href=${item.link}>Подробнее</a>
//                 </div>
//             `;

//             document.querySelector(wrapper).appendChild(card);
//         });
//     };
// };

